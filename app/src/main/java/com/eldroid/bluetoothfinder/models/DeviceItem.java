package com.eldroid.bluetoothfinder.models;

import android.telephony.SignalStrength;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Vujica on 17-Apr-17.
 */

public abstract class DeviceItem implements Comparable<DeviceItem>,Serializable{

    //private ArrayList<Double>signalStrengthList;
    public String mName;
    public String mAddress;
    public int mRSSI;

    public DeviceItem(String name, String address, int rssi){
        mAddress=address;
        mName=name;
        mRSSI=rssi;
    }
    public abstract String getmName();
    public abstract String getmAddress();
    public abstract boolean isConnected();
    public  abstract int getSignalStrength();
    public abstract void setSignalStrenght(int signal);
    public abstract void setConneted(boolean conneted);
    public abstract void setDistance(double distance);
    public abstract double getDistance();
    public abstract void setTxPower(int power);
    public abstract int getTxPower();

}
