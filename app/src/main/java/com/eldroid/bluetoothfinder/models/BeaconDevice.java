package com.eldroid.bluetoothfinder.models;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Vujica on 17-Apr-17.
 */

public class BeaconDevice extends DeviceItem implements Comparable<DeviceItem> {
    private String id1,id2,id3;
    private int txPower;
    private double mDistance;

    public BeaconDevice(String id, String name, String address, int rssi) {
        super(name, address, rssi);
    }

    public BeaconDevice(String ID1,String ID2,String ID3, String address,int rssi,  double distance, int TxPower ) {
        super(ID1, address, rssi);
        id1=ID1;
        id2=ID2;
        id3=ID3;
        mDistance = distance;
        txPower=TxPower;

    }

    @Override
    public String getmName() {
        return mName;
    }

    @Override
    public String getmAddress() {
        return mAddress;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public int getSignalStrength() {
        return mRSSI;
    }

    @Override
    public void setSignalStrenght(int signal) {
        this.mRSSI = signal;
    }



    @Override
    public void setConneted(boolean conneted) {

    }

    @Override
    public void setDistance(double distance) {
        mDistance = distance;
    }

    @Override
    public double getDistance() {
        return mDistance;
    }

    @Override
    public void setTxPower(int voltage) {
        txPower=voltage;
    }

    @Override
    public int getTxPower() {
        return txPower;
    }
    public String getId2()
    {
        return id2;
    }
    public String getId3(){
        return id3;
    }
    @Override
    public int compareTo(DeviceItem another) {
        return another.getSignalStrength()-mRSSI;
    }

    @Override
    public boolean equals(Object object) {
        if(object instanceof BeaconDevice)
        {
            BeaconDevice device=(BeaconDevice)object;
            if(device.getmAddress().equals(this.getmAddress())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.mAddress != null ? this.mAddress.hashCode() : 0);
        return hash;

    }
}
