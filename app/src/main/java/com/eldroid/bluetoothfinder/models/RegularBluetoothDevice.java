package com.eldroid.bluetoothfinder.models;


/**
 * Created by Vujica on 18-Apr-17.
 */

public class RegularBluetoothDevice extends DeviceItem implements Comparable<DeviceItem> {
    private boolean mConnected;
    private int mBluetoothClass;


    public RegularBluetoothDevice(String name, String address, boolean connected, int rssi,int bClass) {
        super(name, address,  rssi);
        this.mConnected=connected;
        this.mBluetoothClass=bClass;


    }

    @Override
    public String getmName() {
        return mName;
    }

    @Override
    public String getmAddress() {
        return this.mAddress;
    }

    @Override
    public boolean isConnected() {
        return this.mConnected;
    }

    @Override
    public int getSignalStrength() {
        return this.mRSSI;
    }

    @Override
    public void setSignalStrenght(int signal) {
        this.mRSSI = signal;

    }
    public int getBluetoothClass()
    {
        return this.mBluetoothClass;
    }


    @Override
    public void setConneted(boolean conneted) {
        this.mConnected=conneted;
    }
    /**
     * Regular bluetooth device doesn't have temp, alias,distance, so don't use these methods
     *
     */



    @Override
    public void setDistance(double distance) {

    }

    @Override
    public double getDistance() {
        return 0;
    }

    @Override
    public void setTxPower(int power) {

    }

    @Override
    public int getTxPower() {
        return 0;
    }


    @Override
    public int compareTo(DeviceItem another) {
        return another.getSignalStrength()-mRSSI;
    }
}
