package com.eldroid.bluetoothfinder.fragments.scaning;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.eldroid.bluetoothfinder.R;
import com.eldroid.bluetoothfinder.RadarView.BeaconProvider;
import com.eldroid.bluetoothfinder.fragments.BaseFragment;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import com.eldroid.bluetoothfinder.models.DeviceItem;
import com.eldroid.bluetoothfinder.models.RegularBluetoothDevice;

import java.util.ArrayList;


/**
 * Created by Vujica on 17-Apr-17.
 */

public class ScanFragment extends BaseFragment implements DeviceDiscoveryListener {
    public static final String TAG = ScanFragment.class.getSimpleName();
    RecyclerView rv_devices;
    ProgressBar progressBar;
    private ArrayList<DeviceItem> deviceArrayList;
    private ScanAdapter scanAdapter;
    private RegularBluetoothDevice dev;
    private BeaconProvider beaconProvider =BeaconProvider.getInstance();


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_scanning;
    }

    @Override
    protected void linkUi() {
        rv_devices = (RecyclerView) findViewById(R.id.rv_devices);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void init() {
        deviceArrayList = new ArrayList<>();
        //dev = new RegularBluetoothDevice("helo","1:2:3:4:5:6",false,23,233);
        //deviceArrayList.add(dev);
        rv_devices.setLayoutManager(new LinearLayoutManager(getContext()));
        scanAdapter = new ScanAdapter(mainActivity, deviceArrayList, new OnItemClickListener() {
            @Override
            public void onItemClick(DeviceItem item) {
                //show detail fragment
                mainActivity.showDetailedView(item);
            }
        });

    }

    @Override
    protected void setData() {
        rv_devices.setHasFixedSize(true);
        rv_devices.setAdapter(scanAdapter);
    }

    @Override
    protected void setActions() {

    }

    @Override
    public void deviceFound(DeviceItem device) {
        if(scanAdapter!=null) {
            scanAdapter.add(device);
            if (device instanceof BeaconDevice)
            {
                beaconProvider.addBeacon((BeaconDevice) device);
            }
        }
    }

    @Override
    public void scanCompleted() {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
        if(mainActivity!=null)
        Toast.makeText(mainActivity, "Scanning completed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void scanStarted() {
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);
        if(scanAdapter!=null)
        scanAdapter.clearAll();
        if(beaconProvider!=null)
            beaconProvider.getBeaconDevices().clear();

    }



}
