package com.eldroid.bluetoothfinder.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eldroid.bluetoothfinder.MainActivity;

/**
 * Created by Vujica on 17-Apr-17.
 */

public abstract class BaseFragment extends Fragment {
    protected MainActivity mainActivity;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity=(MainActivity)getActivity();
        linkUi();
        init();
        setData();
        setActions();
    }
    protected View findViewById(int id)
    {
        return getView().findViewById(id);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResId(),container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    protected abstract int getLayoutResId();
    protected void init(){};
    protected void setData(){};
    protected void linkUi(){};
    protected void setActions(){};
    protected void clearData(){};

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearData();
    }
}
