package com.eldroid.bluetoothfinder.fragments.scaning;
import android.widget.Filter;

import com.eldroid.bluetoothfinder.models.DeviceItem;

import java.util.ArrayList;

/**
 * Created by Vujica on 19-Apr-17.
 */

public class ConnectedDevicesFilter extends Filter {
    private ScanAdapter scanAdapter;
    private ArrayList<DeviceItem>originalList;
    private ArrayList<DeviceItem>filteredList;

    public ConnectedDevicesFilter(ScanAdapter adapter, ArrayList<DeviceItem> deviceItemArrayList)
    {
        super();
        this.scanAdapter=adapter;
        this.originalList=new ArrayList<>(deviceItemArrayList);
        this.filteredList=new ArrayList<DeviceItem>();

    }

    /**
     *
     * @param charSequence not important, we are not realy performing filtering by name, but by whether or not item is connected to device
     * @return
     */
    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        filteredList.clear();
        final FilterResults results=new FilterResults();
        if(charSequence.length()==0){
            filteredList.addAll(originalList);
        }else{
            for (DeviceItem item : originalList) {
                if (item.isConnected()){
                    filteredList.add(item);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        scanAdapter.clearAll();
        scanAdapter.addAll((ArrayList<DeviceItem>)results.values);
        scanAdapter.notifyDataSetChanged();
    }
}
