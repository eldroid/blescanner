package com.eldroid.bluetoothfinder.fragments.scaning;

import com.eldroid.bluetoothfinder.models.DeviceItem;

/**
 * Created by Vujica on 20-Apr-17.
 */

public interface OnItemClickListener {
    public void onItemClick(DeviceItem item);
}
