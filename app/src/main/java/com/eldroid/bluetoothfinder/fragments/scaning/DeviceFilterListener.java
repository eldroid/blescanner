package com.eldroid.bluetoothfinder.fragments.scaning;

/**
 * Created by Vujica on 19-Apr-17.
 */

public interface DeviceFilterListener {
    public void showNearbyDevices();
    public void showBoundedDevices();
}
