package com.eldroid.bluetoothfinder.fragments.scaning;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.eldroid.bluetoothfinder.R;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import com.eldroid.bluetoothfinder.models.DeviceItem;
import com.eldroid.bluetoothfinder.models.RegularBluetoothDevice;
import java.util.ArrayList;
import java.util.Collections;



/**
 * Created by Vujica on 17-Apr-17.
 */

public class ScanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DeviceItem> deviceArrayList;
    private Context context;
    private ConnectedDevicesFilter filter;
    private final int REGULAR_DEVICE = 0, IBEACON_DEVICE = 1;
    private OnItemClickListener onItemClickListener;
    //minimum signal strength that can be detected in dbm
    // private final int MIN_DBM = -100;

    public ScanAdapter(Context context, ArrayList<DeviceItem> bluetoothDevices,OnItemClickListener clickListener) {
        this.deviceArrayList = bluetoothDevices;
        this.context = context;
        this.onItemClickListener=clickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (deviceArrayList.get(position) instanceof RegularBluetoothDevice) {
            return REGULAR_DEVICE;
        } else if (deviceArrayList.get(position) instanceof BeaconDevice) {
            return IBEACON_DEVICE;
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case REGULAR_DEVICE:
                View view = inflater.inflate(R.layout.scan_item, parent, false);
                viewHolder = new ScanViewHolder(view);
                break;
            case IBEACON_DEVICE:
                View view2 = inflater.inflate(R.layout.ibeacon_item, parent, false);
                viewHolder = new BeaconViewHolder(view2);
                break;
            default:
                View view3 = inflater.inflate(R.layout.ibeacon_item, parent, false);
                viewHolder = new BeaconViewHolder(view3);
               // Toast.makeText(context, "default layout", Toast.LENGTH_SHORT).show();
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case REGULAR_DEVICE:
                final RegularBluetoothDevice device = (RegularBluetoothDevice) deviceArrayList.get(position);
                if (device != null) {
                    ScanViewHolder scanViewHolder = (ScanViewHolder) holder;
                    Log.d("ScanAdapter", "Device signal strength:" + device.getSignalStrength());
                    scanViewHolder.txt_signal.setText(String.valueOf(device.getSignalStrength()));
                    scanViewHolder.txt_name.setText(device.getmName());
                    scanViewHolder.txt_address.setText(device.getmAddress());
                    scanViewHolder.txt_connected.setText(device.isConnected() ? context.getString(R.string.device_conected) : context.getString(R.string.device_unbound));
                    //
//                    scanViewHolder.btn_connect.setVisibility(device.isConnected() ? View.GONE : View.VISIBLE);
                    scanViewHolder.btn_connect.setVisibility(View.GONE);
                    scanViewHolder.layoutHolder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onItemClickListener.onItemClick(device);
                        }
                    });
                }
                break;
            case IBEACON_DEVICE:
                final BeaconDevice beaconDevice = (BeaconDevice) deviceArrayList.get(position);
                if (beaconDevice != null) {
                    BeaconViewHolder beaconViewHolder = (BeaconViewHolder) holder;
                    Log.d("ScanAdapter", "Device signal strength:" + beaconDevice.getSignalStrength());
                    beaconViewHolder.txt_signal.setText(String.valueOf(beaconDevice.getSignalStrength()));
                    if(beaconDevice.getmName()!=null)
                    beaconViewHolder.txt_name.setText(beaconDevice.getmName());
                    beaconViewHolder.txt_address.setText(beaconDevice.getmAddress());
                    beaconViewHolder.txt_distance.setText(String.valueOf(beaconDevice.getDistance()));
                    beaconViewHolder.layoutHolder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onItemClickListener.onItemClick(beaconDevice);
                        }
                    });
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return deviceArrayList.size();
    }



    /**
     * ViewHolder for rendering regular bluetooth device items
     */
    public class ScanViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_name, txt_address, txt_connected, txt_signal;
        private Button btn_connect;
        private ConstraintLayout layoutHolder;

        public ScanViewHolder(View itemView) {
            super(itemView);
            layoutHolder=(ConstraintLayout) itemView.findViewById(R.id.constraintItemHolder);
            txt_name = (TextView) itemView.findViewById(R.id.txt_beaconName);
            txt_address = (TextView) itemView.findViewById(R.id.txt_beaconAddress);
            txt_connected = (TextView) itemView.findViewById(R.id.txt_deviceConnectedStatus);
            txt_signal = (TextView) itemView.findViewById(R.id.txt_beaconSignal);
            btn_connect = (Button) itemView.findViewById(R.id.btn_connect);
        }

    }

    /**
     * ViewHolder for displaying beacon items
     */
    public class BeaconViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout layoutHolder;
        private TextView txt_name, txt_address, txt_distance, txt_txPower, txt_signal,txt_id2,txt_id3;;

        public BeaconViewHolder(View itemView) {
            super(itemView);
            layoutHolder=(ConstraintLayout)itemView.findViewById(R.id.constraintIBeaconHolder);
            txt_signal = (TextView) itemView.findViewById(R.id.txt_iBeaconSignal);
            txt_name = (TextView) itemView.findViewById(R.id.txt_iBeaconName);
            txt_address = (TextView) itemView.findViewById(R.id.txt_iBeaconAddress);
            txt_distance = (TextView) itemView.findViewById(R.id.txt_beaconDistanceValue);
//            txt_txPower = (TextView) itemView.findViewById(R.id.txt_txPowerValue);
//            txt_id2 = (TextView) itemView.findViewById(R.id.txt_Id2Value);
//            txt_id3 = (TextView) itemView.findViewById(R.id.txt_Id3Value);

        }
    }

    public void addAll(ArrayList<DeviceItem> alldevices) {
        deviceArrayList = new ArrayList<>(alldevices);
        Collections.sort(deviceArrayList);
        notifyDataSetChanged();
    }

    /**
     * Adds item to arraylist
     * if item is already there, change values to reflect the new ones
     * if item discovered is a beacon and we have a hit in arraylist, check if a device that is already in list matches the type BeaconDevice
     * if not, make beacon device and replace old one in the list.
     * Sometimes beacons are detected and added into list before library picks them up, and we have regular devices inside.
     * Must check for type and replace if necessary
     *
     *@param item - item to be added in the list
     */
    public void add(DeviceItem item) {
        boolean itemExists=false;
        int pos=0;
      for(DeviceItem deviceItem:deviceArrayList)
        {
            if(item instanceof BeaconDevice){
                if(deviceItem.getmAddress().equals(item.getmAddress())){
                    deviceItem.setSignalStrenght(item.getSignalStrength());
                    deviceItem.setConneted(item.isConnected());
                    if(deviceItem instanceof BeaconDevice){
                        deviceItem.setDistance(item.getDistance());
                        deviceItem.setTxPower(item.getTxPower());
                    }
                    else{
                        deviceItem=new BeaconDevice(item.getmName(),((BeaconDevice) item).getId2(),((BeaconDevice) item).getId3(),item.getmAddress(),item.getSignalStrength(),((BeaconDevice) item).getDistance(),item.getTxPower());
                        deviceArrayList.set(pos,deviceItem);
                    }
                    itemExists=true;
                }
            }
            else if(deviceItem.getmAddress().equals(item.getmAddress())){
                deviceItem.setSignalStrenght(item.getSignalStrength());
                deviceItem.setConneted(item.isConnected());
                itemExists=true;
            }
            pos++;
        }
        if(!itemExists)
        {
            deviceArrayList.add(item);
        }
        Collections.sort(deviceArrayList);
        notifyDataSetChanged();

    }

    /** Clears arraylist and calls adapters notifyDataSetChanged
     *
     */
    public void clearAll() {
        deviceArrayList.clear();
        notifyDataSetChanged();
    }

}
