package com.eldroid.bluetoothfinder.fragments.scaning;
import com.eldroid.bluetoothfinder.models.DeviceItem;


/**
 * Created by Vujica on 18-Apr-17.
 */

public interface DeviceDiscoveryListener {
    public void deviceFound(DeviceItem device);
    public void scanCompleted();
    public void scanStarted();

}
