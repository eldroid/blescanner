package com.eldroid.bluetoothfinder.fragments.detail;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eldroid.bluetoothfinder.DetailActivity;
import com.eldroid.bluetoothfinder.R;
import com.eldroid.bluetoothfinder.fragments.BaseFragment;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import com.eldroid.bluetoothfinder.models.DeviceItem;
import com.eldroid.bluetoothfinder.models.RegularBluetoothDevice;

import java.lang.reflect.Method;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Vujica on 20-Apr-17.
 */

public class DetailFragment extends Fragment {
    public static String TAG = BaseFragment.class.getSimpleName();
    private ImageView img_device;
    private TextView txt_NameOrId, txt_device, txt_FieldOne, txt_FieldOneValue, txt_FieldTwo, txt_FieldTwoValue, txt_FieldThree, txt_FieldThreeValue;
    private TextView txt_FieldFour, txt_FieldFourValue, txt_FieldFive, txt_FieldFiveValue, txt_FieldSix, txt_FieldSixValue;
    private String name, address, power, id2, id3, distance;
    private int rssi;
    private Button btn_pair;
    private DeviceItem item;
    private Bundle args;
    private Activity activity;
    private boolean isPaired = false;
    private static int BLUETOOTH_SETTINGS=1;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity=getActivity();
        linkUi();
        init();
        setData();
        setActions();
    }

    protected View findViewById(int id) {
        return getView().findViewById(id);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    protected void linkUi() {
        txt_NameOrId = (TextView) findViewById(R.id.txt_deviceNameOrId);
        txt_device = (TextView) findViewById(R.id.txt_deviceName);
        img_device = (ImageView) findViewById(R.id.img_device);
        txt_FieldOne = (TextView) findViewById(R.id.txt_field1);
        txt_FieldOneValue = (TextView) findViewById(R.id.txt_field1Value);
        txt_FieldTwo = (TextView) findViewById(R.id.txt_field2);
        txt_FieldTwoValue = (TextView) findViewById(R.id.txt_field2Value);
        txt_FieldThree = (TextView) findViewById(R.id.txt_field3);
        txt_FieldThreeValue = (TextView) findViewById(R.id.txt_field3Value);
        txt_FieldFour = (TextView) findViewById(R.id.txt_field4);
        txt_FieldFourValue = (TextView) findViewById(R.id.txt_field4Value);
        txt_FieldFive = (TextView) findViewById(R.id.txt_field5);
        txt_FieldFiveValue = (TextView) findViewById(R.id.txt_field5Value);
        txt_FieldSix = (TextView) findViewById(R.id.txt_field6);
        txt_FieldSixValue = (TextView) findViewById(R.id.txt_field6Value);
        btn_pair = (Button) findViewById(R.id.btn_pair);
    }


    protected void init() {
        args = getArguments();
    }

    protected void setData() {
        if (args != null) {
            item = (DeviceItem) args.get("device");
            if (item != null) {
                name = item.getmName();
                rssi = item.getSignalStrength();
                if (item instanceof RegularBluetoothDevice) {
                    if (item.isConnected())
                        isPaired = true;
                    setRegularLayout();
                } else if (item instanceof BeaconDevice) {
                    setBeaconLayout();
                }
            }
        }
    }

    private void setBeaconLayout() {
        BeaconDevice beaconDevice = (BeaconDevice) item;
        txt_NameOrId.setText(name);
        txt_device.setText(R.string.txt_fragmentBeaconId);
        img_device.setImageResource(R.mipmap.ic_beacon);
        txt_FieldOne.setText(R.string.rssi);
        txt_FieldOneValue.setText(String.valueOf(rssi));
        txt_FieldTwo.setText(R.string.id2);
        txt_FieldTwoValue.setText(beaconDevice.getId2());
        txt_FieldThree.setText(R.string.id3);
        txt_FieldThreeValue.setText(beaconDevice.getId3());
        txt_FieldFour.setText(R.string.macadress);
        txt_FieldFourValue.setText(item.getmAddress());
        txt_FieldFive.setText(R.string.txt_distance);
        txt_FieldFiveValue.setText(String.valueOf(beaconDevice.getDistance()));
        txt_FieldSix.setText(R.string.txt_txPower);
        txt_FieldSixValue.setText(String.valueOf(beaconDevice.getTxPower()));

    }

    private void setRegularLayout() {
        RegularBluetoothDevice regularBluetoothDevice = (RegularBluetoothDevice) item;
        txt_NameOrId.setText(name);
        txt_device.setText(R.string.txt_fragmentDeviceName);
        img_device.setImageResource(R.mipmap.ic_bluetooth);
        txt_FieldOne.setText(R.string.rssi);
        txt_FieldOneValue.setText(String.valueOf(rssi));
        txt_FieldTwo.setText(R.string.macadress);
        txt_FieldTwoValue.setText(regularBluetoothDevice.getmAddress());
        txt_FieldThree.setText(R.string.txt_connected);
        txt_FieldThreeValue.setText(regularBluetoothDevice.isConnected() ? R.string.device_conected : R.string.device_unbound);
        txt_FieldFour.setText("Bluetooth Class:");
        txt_FieldFourValue.setText(String.valueOf(regularBluetoothDevice.getBluetoothClass()));
        txt_FieldFive.setVisibility(View.GONE);
        txt_FieldFiveValue.setVisibility(View.GONE);
        txt_FieldSix.setVisibility(View.GONE);
        txt_FieldSixValue.setVisibility(View.GONE);
        btn_pair.setEnabled(true);
        btn_pair.setVisibility(View.VISIBLE);
    }

public void setActions(){

btn_pair.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intentOpenBluetoothSettings = new Intent();
        intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivity(intentOpenBluetoothSettings);
    }
});
}

}
