package com.eldroid.bluetoothfinder;

import com.eldroid.bluetoothfinder.models.BeaconDevice;

/**
 * Created by Vujica on 06-May-17.
 */

public interface MapBeaconFacade {
    public void addBeacon(BeaconDevice device);
    public void updateBeacon(BeaconDevice devicetoUpdate,BeaconDevice freshBeacon);
}
