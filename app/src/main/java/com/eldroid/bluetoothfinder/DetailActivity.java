package com.eldroid.bluetoothfinder;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.eldroid.bluetoothfinder.fragments.detail.DetailFragment;


/**
 * Created by Vujica on 20-Apr-17.
 */

public class DetailActivity extends AppCompatActivity {
    public static String TAG = DetailActivity.class.getSimpleName();
    private FragmentManager fragmentManager;
    private Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        linkUi();
        init();
        setData();
        setActions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void init() {
        bundle = getIntent().getExtras();
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    // link any ui elements needed in activity
    public void linkUi() {
    }

    public void setData() {
        //show detailed fragment
        if(bundle!=null) {
            DetailFragment df = new DetailFragment();
            df.setArguments(bundle);
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainLinearLayout,df,df.getTag());
            fragmentTransaction.commit();
        }
    }

    public void setActions() {

    }


}
