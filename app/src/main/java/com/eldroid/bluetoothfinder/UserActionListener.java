package com.eldroid.bluetoothfinder;

import com.eldroid.bluetoothfinder.models.DeviceItem;

/**
 * Created by Vujica on 18-Apr-17.
 * Interface for starting/stoping scanning
 */

public interface UserActionListener {
    public void startScaning();
    public void stopScaning();
    public void showDetailedView(DeviceItem item);
}
