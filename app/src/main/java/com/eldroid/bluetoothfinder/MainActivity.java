package com.eldroid.bluetoothfinder;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.eldroid.bluetoothfinder.RadarView.RadarViewActivity;
import com.eldroid.bluetoothfinder.dialogs.DialogActionListener;
import com.eldroid.bluetoothfinder.dialogs.YesNoDialogBuilder;
import com.eldroid.bluetoothfinder.fragments.detail.DetailFragment;
import com.eldroid.bluetoothfinder.fragments.scaning.ScanFragment;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import com.eldroid.bluetoothfinder.models.DeviceItem;
import com.eldroid.bluetoothfinder.models.RegularBluetoothDevice;

import org.altbeacon.beacon.AltBeacon;
import org.altbeacon.beacon.AltBeaconParser;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BleNotAvailableException;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.simulator.StaticBeaconSimulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements UserActionListener, BeaconConsumer {
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int REQUEST_BLUETOOTH = 2;
    /**
     * time to wait in milliseconds after scan has completed
     */
    private static final int SCANING_FREQ = 5000;
    private boolean hasAskedForPermission = false;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    private static final String TAG = MainActivity.class.getSimpleName();
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;
    private FragmentManager fragmentManager;
    private BluetoothAdapter bluetoothAdapter;
    private IntentFilter filter;
    private BroadcastReceiver mReceiver;
    private RadioButton btn_near, btn_bounded;
    private FloatingActionButton fab_scan;
    private boolean isScanning = false;
    private CoordinatorLayout coordinatorLayout;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private BeaconManager beaconManager;
    private ViewPager viewPager;
    private MainViewPagerAdapter mainViewPagerAdapter;
    private boolean turnBluetoothOff = false;
    private StaticBeaconSimulator staticBeaconSimulator;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /********************* SETING UI*************************/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        btn_bounded = (RadioButton) findViewById(R.id.btn_connected);
        btn_near = (RadioButton) findViewById(R.id.btn_nearby);
        fab_scan = (FloatingActionButton) findViewById(R.id.fab_scan);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayoutMain);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        viewPager = (ViewPager) findViewById(R.id.vp_main);
        btn_near.setChecked(true);
        Menu menuNav = navigationView.getMenu();
        MenuItem nav_item1 = menuNav.findItem(R.id.drawer_filter);
        nav_item1.setChecked(true);
        /********************************************************/
        verifyBluetooth();
        beaconManager = BeaconManager.getInstanceForApplication(this);
        // TESTING BEACONS REMOVE FROM PRODUCTION
//        staticBeaconSimulator = new StaticBeaconSimulator();
//        setBeaconsTest();
//        BeaconManager.setBeaconSimulator(staticBeaconSimulator);

        //ibeacon parser
        BeaconParser iBeaconParser = new BeaconParser();
        //  Estimote > 2013
        iBeaconParser.setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24");
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.getBeaconParsers().add(iBeaconParser);
        beaconManager.bind(this);
        fragmentManager = getSupportFragmentManager();
        mainViewPagerAdapter = new MainViewPagerAdapter(fragmentManager);
        viewPager.setAdapter(mainViewPagerAdapter);
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            new AlertDialog.Builder(this)
                    .setTitle("Not compatible")
                    .setMessage("Your phone does not support Bluetooth")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        } else {
            if (!bluetoothAdapter.isEnabled())
                enableBluetooth();
            else
                bluetoothAdapter.startDiscovery();
        }
        //Receiver for registering bluetooth discovery
        // When discovery finds a device
// Get the object from the Intent
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                    if (state == BluetoothAdapter.STATE_ON) {
                        Toast.makeText(MainActivity.this, "Bluetooth turned on", Toast.LENGTH_SHORT).show();
                    }
                }
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // When discovery finds a device
                    // Get the object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    DeviceItem deviceItem = new RegularBluetoothDevice(device.getName(), device.getAddress(), device.getBondState() == BluetoothDevice.BOND_BONDED, intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE), device.getBluetoothClass().getDeviceClass());
                    try {
                        //Send item to fragments bounded and near
                        MainViewPagerAdapter ma = (MainViewPagerAdapter) (viewPager.getAdapter());
                        ScanFragment nearFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.NEAR);
                        nearFragment.deviceFound(deviceItem);
                        if (deviceItem.isConnected()) {
                            ScanFragment boundedFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.BOUNDED);
                            boundedFragment.deviceFound(deviceItem);
                        }
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Error setting data to fragments", Toast.LENGTH_SHORT).show();
                    }

                    Log.d(TAG, "Found device:" + device.getName() + " with address:" + device.getAddress());
                }
                if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    BluetoothFinderApplication.scaningFinished = true;
                    MainViewPagerAdapter ma = (MainViewPagerAdapter) (viewPager.getAdapter());
                    ScanFragment nearFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.NEAR);
                    ScanFragment boundedFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.BOUNDED);
                    if (nearFragment != null)
                        nearFragment.scanCompleted();
                    if (boundedFragment != null)
                        boundedFragment.scanCompleted();
                    onScanFinished();
                }
                if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                    BluetoothFinderApplication.scaningFinished = false;
                    MainViewPagerAdapter ma = (MainViewPagerAdapter) (viewPager.getAdapter());
                    ScanFragment nearFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.NEAR);
                    ScanFragment boundedFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.BOUNDED);
                    if (nearFragment != null)
                        nearFragment.scanStarted();
                    if (boundedFragment != null)
                        boundedFragment.scanStarted();
                    onScanStarted();
                }
            }
        };
        filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);
        setActions();


        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {


                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.drawer_filter:
                        // Toast.makeText(getApplicationContext(), "Inbox Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.drawer_proximity:
                        Intent intent=new Intent(MainActivity.this,ProximityActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.drawer_mapVIew:
                        Log.d(TAG, "Show location fragment/start scanning");
                        Intent intentMap=new Intent(MainActivity.this,RadarViewActivity.class);
                        startActivity(intentMap);
                        return true;
                    case R.id.drawer_about:
                        // Toast.makeText(getApplicationContext(), "All Mail Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.drawer_feedback:
                        // Toast.makeText(getApplicationContext(), "Trash Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        disableDrawerItems();


    }


    @Override
    protected void onResume() {
        super.onResume();
        checkPermissions();
        registerReceiver(mReceiver, filter);
        beaconManager.bind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        beaconManager.unbind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"On Destroy called");
        if (mReceiver != null)
            unregisterReceiver(mReceiver);
        beaconManager.unbind(this);
        //turn off bluetooth if it was off before entering application
        if (turnBluetoothOff) {
            if (bluetoothAdapter != null)
                bluetoothAdapter.disable();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_locate);
        Drawable icon = getResources().getDrawable(R.mipmap.ic_scan);
        icon.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        item.setIcon(icon);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_locate:
                Log.d(TAG, "Show location fragment/start scanning");
                Intent intent=new Intent(MainActivity.this,RadarViewActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_settings:
                Log.d(TAG, "Show settings fragment");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * sends intent for user to turn on bluetooth on device
     */
    public void enableBluetooth() {
        Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBT, REQUEST_BLUETOOTH);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_BLUETOOTH:
                if (resultCode == RESULT_OK) {
                    if (bluetoothAdapter.isEnabled()) {
                        BluetoothFinderApplication.isBluetoothOn = true;
                        turnBluetoothOff = true;
                        bluetoothAdapter.startDiscovery();
                    }
                } else {
                    if (!bluetoothAdapter.isEnabled())
                        BluetoothFinderApplication.isBluetoothOn = false;
                }
                break;
        }
    }

    public void checkPermissions() {
        if (!hasAskedForPermission)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Android M Permission check 
                if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    final YesNoDialogBuilder builder = new YesNoDialogBuilder(this);
                    builder.setTitle(R.string.dialog_permission_title);
                    builder.setMessage(R.string.dialog_permission_message);
                    builder.setPositiveButtonText(android.R.string.ok);
                    builder.setListener(new DialogActionListener() {
                        @Override
                        public void onDismiss() {
                            Toast.makeText(MainActivity.this, "App needs bluetooth permision", Toast.LENGTH_SHORT).show();
                            hasAskedForPermission = true;
                        }

                        @Override
                        public void onAccept() {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                            hasAskedForPermission = false;
                        }
                    });
                    builder.show();

                }

            }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Coarse location permission granted");
                } else {
                    //User has not granted the permissions
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.dialog_bluetooth_not_granted_title);
                    builder.setMessage(R.string.dialog_bluetooth_not_granted_message);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    /**
     * starts bluetooth discovery
     */
    @Override
    public void startScaning() {
        if (!bluetoothAdapter.isEnabled()) {
            enableBluetooth();
        } else {
            bluetoothAdapter.startDiscovery();
            Log.d(TAG, "start scanning");
        }
    }

    /**
     * stops bluetooth discovery
     */
    @Override
    public void stopScaning() {
        bluetoothAdapter.cancelDiscovery();
    }

    @Override
    public void showDetailedView(DeviceItem item) {
        Bundle extras = new Bundle();
        extras.putSerializable("device", item);
        Intent startDetailActivity = new Intent(MainActivity.this, DetailActivity.class);
        startDetailActivity.putExtras(extras);
        startActivity(startDetailActivity);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Setting up actions after activity is created, react on user behavior,etc
     */
    public void setActions() {
        btn_near.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    BluetoothFinderApplication.filterNerby = true;
                    viewPager.setCurrentItem(MainViewPagerAdapter.NEAR);
                } else {
                    BluetoothFinderApplication.filterNerby = false;
                    viewPager.setCurrentItem(MainViewPagerAdapter.BOUNDED);
                }
            }
        });
        //fab button on click behaviour
        fab_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isScanning) {
                    onScanStarted();
                    startScaning();
                } else {
                    stopScaning();
                    onScanFinished();

                }
            }
        });
        //when scrolling left and right set top buttons check values based on paga selected
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                        switch (position){
                            case 0:
                                btn_near.setChecked(true);
                                btn_bounded.setChecked(false);
                                break;
                            case 1:
                                btn_near.setChecked(false);
                                btn_bounded.setChecked(true);
                                break;
                        }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void onScanFinished() {
        //When scanning finished set img back to normal
        isScanning = false;
        fab_scan.setImageResource(android.R.drawable.ic_menu_search);
    }

    public void onScanStarted() {
        isScanning = true;
        btn_near.setChecked(true);
        Snackbar.make(coordinatorLayout, R.string.startScan, Snackbar.LENGTH_LONG).show();
        fab_scan.setImageResource(android.R.drawable.ic_media_pause);
    }

    /**
     * Reacts to beacons once they are discovered
     */
    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setForegroundBetweenScanPeriod(SCANING_FREQ);
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                for (Beacon beacon : collection) {
                    Log.d(TAG, "I see a beacon:" + beacon.getBluetoothName() + " aproximatly" + beacon.getDistance() + "meters away");
                    List<Long> datafileds = beacon.getDataFields();
                    final BeaconDevice beaconDevice = new BeaconDevice(beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString(), beacon.getBluetoothAddress(), beacon.getRssi(), beacon.getDistance(), beacon.getTxPower());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //We only send beacons to near fragment, cause u cant connect to them and there is no point on adding them
                            // to bounded list
                            MainViewPagerAdapter ma = (MainViewPagerAdapter) (viewPager.getAdapter());
                            ScanFragment nearFragment = (ScanFragment) ma.getItem(MainViewPagerAdapter.NEAR);
                            if (nearFragment != null)
                                nearFragment.deviceFound(beaconDevice);
                        }
                    });

                }
            }
        });
        //beacon monitoring not needed for now
        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {

            }

            @Override
            public void didExitRegion(Region region) {

            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {

            }
        });
        try {
            beaconManager.startRangingBeaconsInRegion(new Region(BluetoothFinderApplication.UNIQUE_ID, null, null, null));
            beaconManager.startMonitoringBeaconsInRegion(new Region(BluetoothFinderApplication.UNIQUE_ID, null, null, null));
        } catch (RemoteException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    //Verify if we have ble option on device
    public void verifyBluetooth() {
        try {
            if (!BeaconManager.getInstanceForApplication(this).checkAvailability()) {
                // enableBluetooth();
                Log.d(TAG, "Bluetooth is turned off");
            }
            BluetoothFinderApplication.isBleCompatible = true;

        } catch (BleNotAvailableException e) {
            BluetoothFinderApplication.isBleCompatible = false;
            Log.d("BleNotAvailable", e.getMessage());
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.ble_not_available));
            builder.setMessage(getString(R.string.ble_notavailable_message));
            builder.setPositiveButton(android.R.string.ok, null);
            builder.show();
        }
    }

    /**
     * Disables items on drawer cause we did'nt implement them yet
     */
    private void disableDrawerItems() {
        Menu menuNav = navigationView.getMenu();
        MenuItem nav_item1 = menuNav.findItem(R.id.drawer_about);
        nav_item1.setEnabled(false);
        MenuItem nav_item4 = menuNav.findItem(R.id.drawer_feedback);
        nav_item4.setEnabled(false);

    }

    /**
     * Used for testing beacons discovery
     */
    public void setBeaconsTest()
    {
        Beacon beacon = new AltBeacon.Builder().setId1("bd90f30").setId2("2").setId3("3").setRssi(9)
                .setBeaconTypeCode(5).setTxPower(6).setBluetoothName("xx")
                .setBluetoothAddress("1:2:3:4:5:7").setDataFields(Arrays.asList(100l)).build();
        Beacon beacon2 = new AltBeacon.Builder().setId1("1").setId2("2").setId3("3").setRssi(4)
                .setBeaconTypeCode(5).setTxPower(6).setBluetoothName("xx")
                .setBluetoothAddress("1:2:3:4:5:6").setDataFields(Arrays.asList(100l)).build();
        ArrayList<Beacon> beacons = new ArrayList<Beacon>();
        beacons.add(beacon);
        beacons.add(beacon2);
        staticBeaconSimulator.setBeacons(beacons);
    }

}



