package com.eldroid.bluetoothfinder.dialogs;

/**
 * Created by Vujica on 17-Apr-17.
 */

public interface DialogActionListener {
    public void onDismiss();
    public void onAccept();
}
