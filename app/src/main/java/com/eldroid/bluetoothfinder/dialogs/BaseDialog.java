package com.eldroid.bluetoothfinder.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.WindowCompat;
import android.support.v7.app.AppCompatDialog;
import android.view.Window;

/**
 * Created by Vujica on 17-Apr-17.
 */

public abstract class BaseDialog extends AppCompatDialog {
    public BaseDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(getLayoutResId());
            getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            init();
            linkUi();
            setData();
            setActions();
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }
    protected abstract int getLayoutResId();
    protected void init(){};
    protected void linkUi(){};
    protected void setActions(){};
    protected void setData(){};
    protected void clearData(){};

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearData();
    }
}

