package com.eldroid.bluetoothfinder.dialogs;

import android.content.Context;
import android.support.v7.app.AlertDialog;

public class YesNoDialogBuilder {
    private Context context;
    private CharSequence title;
    private CharSequence message;
    private CharSequence mPositiveButtonText;
    private DialogActionListener listener;

    public YesNoDialogBuilder(Context context)
    {
        this.context=context;
    }

    public YesNoDialogBuilder setTitle(CharSequence title) {
        this.title = title;
        return this;
    }
    public YesNoDialogBuilder setTitle(int titleId)
    {
        this.title=context.getText(titleId);
        return this;
    }

    public YesNoDialogBuilder setMessage(String message) {
        this.message = message;
        return this;
    }
    public YesNoDialogBuilder setMessage(int messageId) {
        message = context.getText(messageId);
        return this;
    }
    public YesNoDialogBuilder setPositiveButtonText(CharSequence btnText)
    {
        mPositiveButtonText=btnText;
        return this;
    }
    public YesNoDialogBuilder setPositiveButtonText(int resId){
        mPositiveButtonText=context.getText(resId);
        return this;
    }

    /**
     * Set the message to display.
     *
     * @return This Builder object to allow for chaining of calls to set methods
     */
    public YesNoDialogBuilder setMessage(CharSequence message) {
        this.message=message;
        return this;
    }

    public YesNoDialogBuilder setListener(DialogActionListener listener) {
        this.listener = listener;
        return this;
    }

    public YesNoDialog show() {
        YesNoDialog dialog=new YesNoDialog(context, title, message,mPositiveButtonText, listener);
        dialog.show();
        return dialog;
    }
}