package com.eldroid.bluetoothfinder.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eldroid.bluetoothfinder.R;

/**
 * Created by Vujica on 17-Apr-17.
 */

public class YesNoDialog extends BaseDialog {
    private Context context;
    private Button btn_yes,btn_no;
    private TextView txt_title,txt_message;
    private CharSequence message,title,positiveBtnText;
    private DialogActionListener dialogActionListener;
    @Override
    protected int getLayoutResId() {
        return R.layout.yes_no_dialog;
    }
    public YesNoDialog(Context context, CharSequence title, CharSequence message,CharSequence positiveBtn,DialogActionListener listener)
    {
        super(context);
        this.context=context;
        this.message=message;
        this.title=title;
        this.positiveBtnText=positiveBtn;
        dialogActionListener=listener;
    }

    @Override
    protected void init() {
        super.init();
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dismiss();
                if(dialogActionListener!=null)
                dialogActionListener.onDismiss();

            }
        });
    }

    @Override
    protected void linkUi() {
        super.linkUi();
        btn_yes=(Button)findViewById(R.id.btn_ok);
        btn_no=(Button)findViewById(R.id.btn_cancel);
        txt_message=(TextView)findViewById(R.id.txt_message);
        txt_title=(TextView)findViewById(R.id.txt_title);
    }

    @Override
    protected void setData() {
        if(positiveBtnText!=null)
        btn_yes.setText(positiveBtnText);
        txt_title.setText(title);
        txt_message.setText(message);
    }

    @Override
    protected void setActions() {

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(dialogActionListener!=null)
                dialogActionListener.onAccept();

            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(dialogActionListener!=null)
                dialogActionListener.onDismiss();

            }
        });

    }





}
