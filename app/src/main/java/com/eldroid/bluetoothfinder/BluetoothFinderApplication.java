package com.eldroid.bluetoothfinder;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import com.estimote.sdk.EstimoteSDK;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Vujica on 17-Apr-17.
 */

public class BluetoothFinderApplication extends Application{
    public static boolean isBluetoothOn=false;
    public static boolean scaningFinished=true;
    public static final String UNIQUE_ID="com.eldroid.bluetoothScanner";
    public static boolean isBleCompatible=false;
    public static boolean filterNerby=true;
    //public static CopyOnWriteArrayList<BeaconDevice>beaconDevices=new CopyOnWriteArrayList<>();
    @Override
    public void onCreate() {
        super.onCreate();
        //Initialize fabric
        Fabric.with(this, new Crashlytics());
        EstimoteSDK.initialize(getApplicationContext(), "<#App ID#>", "<#App Token#>");

       // beaconDevices.addAll(Arrays.asList(bd1,bd2,bd3,bd4));
    }






}
