package com.eldroid.bluetoothfinder.RadarView;

import com.eldroid.bluetoothfinder.MapBeaconFacade;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Vujica on 06-May-17.
 */

public class BeaconProvider implements MapBeaconFacade{
    private static BeaconProvider instance;
    private static CopyOnWriteArrayList<BeaconDevice> beaconDevices=new CopyOnWriteArrayList<>();
    BeaconDevice bd1=new BeaconDevice("id1","","","AD:23:21:22:3a",-44,1.334444,-24);
    BeaconDevice bd2=new BeaconDevice("id3","","","M3:FF:21:22:3a",-44,2.0000,-24);
    BeaconDevice bd3=new BeaconDevice("id888","","","Aq:00:21:22:3a",-44,4.334444,-24);
    BeaconDevice bd4=new BeaconDevice("far","","","Aq:aa:21:22:3a",-44,20.444,-24);
    private BeaconProvider()
    {

    }

    public synchronized static BeaconProvider getInstance()
    {
        if(instance==null)
            instance=new BeaconProvider();
        return instance;
    }

    @Override
    public void addBeacon(BeaconDevice device) {

        boolean exists=false;
        for(BeaconDevice beaconDevice:beaconDevices)
        {
            if(beaconDevice.equals(device))
            {
                //update beacon
                updateBeacon(beaconDevice,device);
                exists=true;
            }
        }
        if(!exists)
        {
            beaconDevices.add(device);
        }

    }

    @Override
    public void updateBeacon(BeaconDevice deviceToUpdate, BeaconDevice newBeacon) {
        if(deviceToUpdate!=null) {
            deviceToUpdate.setDistance(newBeacon.getDistance());
            deviceToUpdate.setConneted(newBeacon.isConnected());
            deviceToUpdate.setSignalStrenght(newBeacon.getSignalStrength());
        }
    }
    public CopyOnWriteArrayList<BeaconDevice>getBeaconDevices()
    {
        return beaconDevices;
    }
    public void addAllBeacons(CopyOnWriteArrayList<BeaconDevice> beacons)
    {
        if(beaconDevices!=null)
            beaconDevices.clear();
        beaconDevices.addAll(beacons);
    }


}
