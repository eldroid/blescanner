package com.eldroid.bluetoothfinder.RadarView;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.eldroid.bluetoothfinder.DetailActivity;
import com.eldroid.bluetoothfinder.R;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Vujica on 04-May-17.
 */

public class RadarViewActivity extends AppCompatActivity implements BeaconConsumer {
    private static String TAG = RadarViewActivity.class.getSimpleName();
    private static final int SCANING_FREQ = 5000;//in ms
    private final int REFRESH_INTERVAL = 6000;//in ms
    private final int ANIM_DURATION = 500;//in ms
    private LottieAnimationView radarView;
    private FrameLayout frameLayout;
    ConcurrentHashMap<String, Point> pointConcurrentHashMap = new ConcurrentHashMap<>();
    ConcurrentHashMap<String, View> viewHashMap = new ConcurrentHashMap<>();
    private float circleRad;
    private Timer timer;
    private TimedTask timedTask;
    private int width;
    private int height;
    BeaconManager beaconManager;
    BeaconProvider beaconProvider;
    private Toolbar toolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radarview);
        radarView = (LottieAnimationView) findViewById(R.id.animationRadarView);
        frameLayout = (FrameLayout) findViewById(R.id.frameRadar);
        toolbar=(Toolbar)findViewById(R.id.toolbarRadar);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;
        setActions();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_mapView);
        BeaconParser iBeaconParser = new BeaconParser();
        //  Estimote > 2013
        iBeaconParser.setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24");
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.getBeaconParsers().add(iBeaconParser);
        beaconManager.bind(this);
        beaconProvider=BeaconProvider.getInstance();

    }

    public void setActions() {
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        radarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ViewTreeObserver viewTreeObserver = frameLayout.getViewTreeObserver();
        if (viewTreeObserver != null) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    circleRad = (float) Math.floor((double) ((frameLayout.getWidth() / 18) / 2));
                }
            });
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                timer = new Timer();
                timedTask = new TimedTask();
                timer.schedule(timedTask, 100, REFRESH_INTERVAL);
            }
        }, 1000);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_info:
                showInfoDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_child, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_info);
        Drawable icon = getResources().getDrawable(R.mipmap.ic_information);
        icon.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        item.setIcon(icon);
        return true;
    }

    public void displayBeacons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Displaying beacons...");
                if (beaconProvider.getBeaconDevices() != null && beaconProvider.getBeaconDevices().size() > 0) {
//                    if (pointConcurrentHashMap != null && pointConcurrentHashMap.size() > 0) {
//                        pointArrayList.clear();
//                    }
                    CopyOnWriteArrayList<BeaconDevice> copyOnWriteArrayList = beaconProvider.getBeaconDevices();
                    if (copyOnWriteArrayList != null && copyOnWriteArrayList.size() > 0) {
                        Iterator it = copyOnWriteArrayList.iterator();
                        while (it.hasNext()) {
                            BeaconDevice beaconDevice = (BeaconDevice) it.next();
                            if (beaconDevice != null) {
                                View view;
                                Point point;
                                if (viewHashMap != null && viewHashMap.containsKey(beaconDevice.getmAddress())) {
                                    view = (View) viewHashMap.get(beaconDevice.getmAddress());
                                    TextView textView = (TextView) view.findViewWithTag(beaconDevice);
                                    if (beaconDevice.getmName() == null || beaconDevice.getmName().equalsIgnoreCase("")) {
                                        textView.setText("N/A\n");

                                    } else {
                                        textView.setText(String.valueOf(beaconDevice.getSignalStrength()));
                                        view.invalidate();
                                    }
                                    try {
                                        point = getCoordinates(beaconDevice);
                                        AnimatorSet animatorSet = new AnimatorSet();
                                        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationY", pointConcurrentHashMap.get(beaconDevice.getmAddress()).y - circleRad, ((float) point.y - circleRad));
                                        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "translationX", pointConcurrentHashMap.get(beaconDevice.getmAddress()).x - circleRad, ((float) point.x - circleRad));
                                        animatorSet.playTogether(new Animator[]{ofFloat2, ofFloat});
                                        animatorSet.setDuration(ANIM_DURATION);
                                        animatorSet.setInterpolator(new LinearInterpolator());
                                        animatorSet.start();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    view = makeCircle(beaconDevice);
                                    point = getCoordinates(beaconDevice);
                                    if (point != null) {
                                        view.setX(((float) point.x) - circleRad);
                                        view.setY(((float) point.y) - circleRad);
                                    }
                                    frameLayout.addView(view);
                                    viewHashMap.put(beaconDevice.getmAddress(), view);
                                }

                            }
                        }
                    }
                } else {
                    //underlying data is empty, so we should clear views
                    frameLayout.removeAllViews();
                    pointConcurrentHashMap.clear();
                    viewHashMap.clear();
                }
            }
        });
    }

    public View makeCircle(final BeaconDevice device) {
        RelativeLayout.LayoutParams layoutParams;
        RelativeLayout relativeLayout = new RelativeLayout(this);
        layoutParams = new RelativeLayout.LayoutParams(((int) circleRad * 4), ((int) circleRad * 4));
        relativeLayout.setLayoutParams(layoutParams);
        CircleView circleFillView = null;
        if (device != null) {
            circleFillView = new CircleView((Context) this);
            circleFillView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            relativeLayout.addView(circleFillView, layoutParams);
        }
        TextView textView = new TextView(this);
        if (device != null) {
            textView.setTag(device);
        }
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT);
        textView.setLayoutParams(layoutParams2);
        if (!(device == null)) {
            textView.setText(String.valueOf(device.getSignalStrength()));

        }
        textView.setTextColor(getResources().getColor(R.color.white));
        relativeLayout.addView(textView);

        circleFillView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = new Bundle();
                extras.putSerializable("device", device);
                Intent intent = new Intent(RadarViewActivity.this, DetailActivity.class);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        return relativeLayout;
    }

    private Point getCoordinates(BeaconDevice device) {
        Point p;
        Resources resources = this.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float distanceFromBottom = resources.getDimension(R.dimen.radarHeight) * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);

        if (!viewHashMap.containsKey(device.getmAddress())) {
            Random r = new Random();
            int x = r.nextInt((int) (width - circleRad));
            if (device.getDistance() < 10) {
                p = new Point(x, height - (int)distanceFromBottom - (int) device.getDistance() * 100);
                if(device.getDistance()<1)
                {
                    p = new Point(p.x, height - (int)distanceFromBottom - (int) device.getDistance() * 100);
                }
            } else {
                int pointHeight = (height - (int)distanceFromBottom - (int) (device.getDistance() * 100 % height));
                if (pointHeight > height / 2)
                    pointHeight = height - pointHeight;
                p = new Point(x, pointHeight);
            }
            pointConcurrentHashMap.put(device.getmAddress(), p);
            return p;
        } else {
            //we have device already in hashmap
            // so we already have it displayed on screen.
            //we want to keep x coordinate, and only change hte y component, if distance is changed since the last time
            p = pointConcurrentHashMap.get(device.getmAddress());
            if (p != null) {
                if (device.getDistance() < 10) {
                    p = new Point(p.x, height - (int)distanceFromBottom - (int) device.getDistance() * 100);
                    if(device.getDistance()<1)
                    {
                        p = new Point(p.x, height - (int)distanceFromBottom - (int) device.getDistance() * 100);
                    }
                }
                else {
                    int pointHeight = (height -(int)distanceFromBottom - (int) (device.getDistance() * 100 % height));
                    if (pointHeight > height / 2)
                        pointHeight = height - pointHeight;
                    p = new Point(p.x, pointHeight);
                }
            }
            return p;
        }
    }

    //Removes beacon from hashmap and the view
    public void removeBeaconFromView(final BeaconDevice beaconDevice) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (viewHashMap != null && viewHashMap.contains(beaconDevice)) {
                    frameLayout.removeView((View) viewHashMap.get(beaconDevice));
                    viewHashMap.remove(beaconDevice);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frameLayout != null)
            frameLayout.removeAllViews();
    }

    @Override
    public void onBeaconServiceConnect() {
            beaconManager.setForegroundBetweenScanPeriod(SCANING_FREQ);
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                for (Beacon beacon : collection) {
                    Log.d(TAG, "I see a beacon:" + beacon.getBluetoothName() + " aproximatly" + beacon.getDistance() + "meters away");
                    List<Long> datafileds = beacon.getDataFields();
                    final BeaconDevice beaconDevice = new BeaconDevice(beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString(), beacon.getBluetoothAddress(), beacon.getRssi(), beacon.getDistance(), beacon.getTxPower());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //We only send beacons to near fragment, cause u cant connect to them and there is no point on adding them
                            // to bounded list
                           beaconProvider.addBeacon(beaconDevice);
                        }
                    });

                }
            }
        });
    }


    class TimedTask extends java.util.TimerTask {

        @Override
        public void run() {
            Log.d(TAG, "Running timed task at system time:" + Calendar.getInstance().getTimeInMillis());
            displayBeacons();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null)
            timer.cancel();
        if (frameLayout != null)
            frameLayout.removeAllViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        beaconManager.bind(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        beaconManager.unbind(this);
    }
    private void showInfoDialog()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setMessage(getString(R.string.dialog_mapView));
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
            }

        });
        builder.show();
    }
}
