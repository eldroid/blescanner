package com.eldroid.bluetoothfinder;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.eldroid.bluetoothfinder.fragments.BaseFragment;
import com.eldroid.bluetoothfinder.fragments.scaning.ScanFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vujica on 21-Apr-17.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    public static final int NEAR=0;
    public static final int BOUNDED=1;
    private List<BaseFragment> fragments=new ArrayList<>();
    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
        ScanFragment neearFragment=new ScanFragment();
        ScanFragment boundedFragment=new ScanFragment();
        addFragment(neearFragment);
        addFragment(boundedFragment);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
    public void addFragment(BaseFragment fragment ) {
        fragments.add(fragment);
    }
}
