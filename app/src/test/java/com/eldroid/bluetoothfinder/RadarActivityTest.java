package com.eldroid.bluetoothfinder;

import com.eldroid.bluetoothfinder.RadarView.BeaconProvider;
import com.eldroid.bluetoothfinder.models.BeaconDevice;
import com.eldroid.bluetoothfinder.models.DeviceItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.CopyOnWriteArrayList;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;


/**
 * Created by Vujica on 06-May-17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RadarActivityTest {
    CopyOnWriteArrayList<BeaconDevice> arrayList = new CopyOnWriteArrayList<>();

    @Before
    public void setup() {
        arrayList.add(new BeaconDevice("1", "2", "AA:BB:DD:CC:00", -24));
        arrayList.add(new BeaconDevice("1", "2", "AA:00:DD:CC:00", -24));
        arrayList.add(new BeaconDevice("1", "2", "AA:BB:FFC:GG:00", -24));
        beaconProvider.addAllBeacons(arrayList);
    }

    @Spy
    BeaconProvider beaconProvider=BeaconProvider.getInstance();

    @Test
    public void shouldAddNewBeacon() {
        BeaconDevice beaconDevice = new BeaconDevice("1", "2", "DD:00:44:22", -21);
        beaconProvider.addBeacon(beaconDevice);
        assertEquals(true, beaconProvider.getBeaconDevices().contains(beaconDevice));

    }
    @Test
    public void shouldOnlyUpdateBeacon()
    {
        System.out.println("sizeBefore:"+beaconProvider.getBeaconDevices().size());
        for(BeaconDevice device:beaconProvider.getBeaconDevices())
        {
            System.out.println(device.getmAddress());
        }
        BeaconDevice beaconDevice = new BeaconDevice("1", "2","3", "AA:BB:DD:CC:00", -21,1.3333,44);
        beaconProvider.addBeacon(beaconDevice);
        System.out.print("sizeAfter:"+beaconProvider.getBeaconDevices().size());
        assertEquals(true,beaconProvider.getBeaconDevices().size()==3);
    }

    @Test
    public void shouldUpdateBeacon(){
        //adding beacon with same mac address as in setup
        BeaconDevice beaconDevice = new BeaconDevice("1", "2","3", "AA:BB:DD:CC:00", -21,1.3333,44);
        beaconProvider.addBeacon(beaconDevice);
        Mockito.verify(beaconProvider).updateBeacon(any(BeaconDevice.class),any(BeaconDevice.class));
    }
}
