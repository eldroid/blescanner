package com.eldroid.bluetoothfinder;
import com.eldroid.bluetoothfinder.models.BeaconDevice;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.*;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by Vujica on 06-May-17.
 */

public class BeaconDeviceTest {
    @Mock
    BeaconDevice beaconDeviceOne;
    @Mock
    BeaconDevice beaconDeviceTwo;

    @Test
    public void shouldPass()
    {
        boolean check = true;
        assertTrue(check);
    }
    @Test
    public void shouldBeEquals()
    {
        beaconDeviceOne=new BeaconDevice("1","name","AD:2a:03:33",34);
        beaconDeviceTwo=new BeaconDevice("122","name2","AD:2a:03:33",34);
        assertEquals(beaconDeviceOne,beaconDeviceTwo);
    }

}
